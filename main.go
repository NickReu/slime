package main

import (
	"github.com/anthonynsimon/bild/adjust"
	"github.com/anthonynsimon/bild/blur"
	"github.com/hajimehoshi/ebiten/v2"
	"github.com/lucasb-eyer/go-colorful"
	"image"
	"log"
	"math"
	"sync"
)

const windowSizeX = 800
const windowSizeY = 500
const scaleFactor = 1
const bounds = 10

var black colorful.Color = colorful.Color{R: 0, G: 0, B: 0}
var white colorful.Color = colorful.Color{R: 1, G: 1, B: 1}
var blue colorful.Color = colorful.Color{R: 0, G: 0.1, B: 0.8}

type Game struct {
	pixels *image.RGBA
	slimes []slime
}

func (g *Game) Update() error {
	var wg sync.WaitGroup
	wg.Add(len(g.slimes))
	for si := range g.slimes {
		go func(si int) {
			defer wg.Done()
			g.slimes[si].update(g.pixels)
		}(si)
	}
	wg.Wait()
	g.pixels = adjust.Brightness(g.pixels, -0.00001)
	g.pixels = blur.Box(g.pixels, 1)
	// log.Println(g.slimes[0].df, g.slimes[0].x, g.slimes[0].y)
	if ebiten.IsKeyPressed(ebiten.KeyDigit0) {
		mx, my := ebiten.CursorPosition()
		for x := 0; x < windowSizeX; x++ {
			for y := 0; y < windowSizeY; y++ {
				if math.Sqrt(float64((x-mx)*(x-mx)+(y-my)*(y-my))) < 20 {
					g.pixels.Set(x, y, white)
				}
			}
		}
	}
	return nil
}

// Draw draws the game screen.
// Draw is called every frame (typically 1/60[s] for 60Hz display).
func (g *Game) Draw(screen *ebiten.Image) {
	// Write your game's rendering.
	screen.Fill(black)
	screen.DrawImage(ebiten.NewImageFromImage(g.pixels), &ebiten.DrawImageOptions{})
}

// Layout takes the outside size (e.g., the window size) and returns the (logical) screen size.
// If you don't have to adjust the screen size with the outside size, just return a fixed size.
func (g *Game) Layout(outsideWidth, outsideHeight int) (screenWidth, screenHeight int) {
	return windowSizeX, windowSizeY
}

func main() {
	// Specify the window size as you like. Here, a doubled size is specified.
	ebiten.SetWindowSize(windowSizeX*scaleFactor, windowSizeY*scaleFactor)
	ebiten.SetWindowTitle("Slime")
	ebiten.SetMaxTPS(30)
	// Call ebiten.RunGame to start your game loop.
	pixs := image.NewRGBA(image.Rectangle{image.Point{0, 0}, image.Point{windowSizeX, windowSizeY}})

	g := &Game{pixs, placeRandomCenter(50000)}

	err := ebiten.RunGame(g)
	if err != nil {
		log.Fatal(err)
	}
}
