module gitlab.com/NickReu/slime

go 1.16

require (
	github.com/anthonynsimon/bild v0.13.0
	github.com/hajimehoshi/ebiten/v2 v2.1.1
	github.com/lucasb-eyer/go-colorful v1.2.0
)
