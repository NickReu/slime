package vec

type Vec [2]float64

func (v Vec) Opposite() Vec {
  return Vec{-v[0],-v[1]}
}

func (v Vec) Plus(u Vec) Vec {
  return Vec{v[0]+u[0],v[1]+u[1]}
}

func (v Vec) Times(n float64) Vec {
  return Vec{v[0]*n,v[1]*n}
}
