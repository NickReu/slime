package main

import (
	"github.com/lucasb-eyer/go-colorful"
	"gitlab.com/NickReu/slime/vec"
	"image"
	"math"
	"math/rand"
)

type slime struct {
	x float64
	y float64
	f float64
	v float64
}

func (s *slime) update(i *image.RGBA) {
	turn := s.calculateTurn(i)
	s.f += turn
	if s.f > 2*math.Pi {
		s.f -= 2 * math.Pi
	} else if s.f < 0 {
		s.f += 2 * math.Pi
	}
	if s.x < bounds {
		// s.x = float64(windowSizeX) - bounds - 1
		s.x = bounds + 1
		s.f = -s.f
	} else if s.x > float64(windowSizeX)-bounds {
		// s.x = bounds + 1
		s.x = float64(windowSizeX) - bounds - 1
		s.f = -s.f
	}
	if s.y < bounds {
		// s.y = float64(windowSizeY) - bounds - 1
		s.y = bounds + 1
		s.f = -s.f + 3*math.Pi
	} else if s.y > float64(windowSizeY)-bounds {
		// s.y = bounds + 1
		s.y = float64(windowSizeY) - bounds - 1
		s.f = -s.f + math.Pi
	}

	// if s.x < 0 || s.x > float64(windowSizeX) || s.y < 0 || s.y > float64(windowSizeY) {
	// 	s.f *= 2 // s.f + math.Pi
	// }
	s.x += math.Sin(s.f) * s.v
	s.y += math.Cos(s.f) * s.v
	c := i.RGBAAt(int(s.x), int(s.y))
	cc, _ := colorful.MakeColor(c)
	i.Set(int(s.x), int(s.y), white.BlendHcl(cc, 0.5))
}

func (s slime) calculateTurn(i *image.RGBA) float64 {
	lpl, lpr := s.calculateLookingPoints()
	lvl := lookValue(lpl, i)
	lvr := lookValue(lpr, i)
	return (rand.Float64()-0.5)/40 + float64(lvr-lvl)/70000
	// if lvl > lvr && lvl-lvr > 10000 {
	// 	return -0.07
	// } else if lvr-lvl > 10000 {
	// 	return 0.07
	// } else {
	// 	return (rand.Float64() - 0.5) / 40
	// }
}

func (s slime) calculateLookingPoints() (vec.Vec, vec.Vec) {
	me := vec.Vec{s.x, s.y}
	sin := math.Sin(s.f)
	cos := math.Cos(s.f)
	forward := vec.Vec{sin, cos}.Times(9)
	left := vec.Vec{-cos, sin}.Times(15)
	right := left.Opposite()
	u := forward.Plus(left).Plus(me)
	v := forward.Plus(right).Plus(me)
	return u, v
}

func lookValue(p vec.Vec, i *image.RGBA) int {
	const radius = 6
	var lightness int
	x0 := p[0] - radius - 1
	xf := p[0] + radius + 1
	y0 := p[1] - radius - 1
	yf := p[1] + radius + 1
	for x := int(x0); x < int(xf); x++ {
		for y := int(y0); y < int(yf); y++ {
			floatX := float64(x)
			floatY := float64(y)
			if math.Sqrt((floatX-p[0])*(floatX-p[0])+(floatY-p[1])*(floatY-p[1])) < radius {
				lightness += int(i.RGBAAt(x, y).R)
				// c := i.RGBAAt(x, y)
				// c.B = 255
				// c.A = 0xff
				// i.Set(x, y, c)
			}
		}
	}
	return lightness
}

func placeRandom(n int) []slime {
	slimes := []slime{}
	for i := 0; i < n; i++ {
		slimes = append(slimes, slime{rand.Float64() * float64(windowSizeX),
			rand.Float64() * float64(windowSizeY), rand.Float64() * 2 * math.Pi, rand.Float64()*2 + 1})
	}
	return slimes
}

func placeRandomForward(n int) []slime {
	slimes := []slime{}
	for i := 0; i < n; i++ {
		slimes = append(slimes, slime{rand.Float64() * float64(windowSizeX),
			rand.Float64() * float64(windowSizeY), math.Pi / 2, rand.Float64()*2 + 1})
	}
	return slimes
}

func placeRandomCenter(n int) []slime {
	slimes := []slime{}
	for i := 0; i < n; i++ {
		slimes = append(slimes, slime{float64(windowSizeX) / 2,
			float64(windowSizeY) / 2, rand.Float64() * 2 * math.Pi, rand.Float64()*5 + 1})
	}
	return slimes
}
